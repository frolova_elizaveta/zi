package freqan;

import java.util.Scanner;

/**
 * Начальный интерфейс
 */
public class StartingPoint {
    /*Конструктор начального интерфейса, который запускается в начале программы*/
    public StartingPoint() {
        System.out.println("0 - Encryption\n" + "1 - Frequency Analysis");
        int userAnswer = readIntData();
        if (userAnswer == 0) {
            new Encryption();
        } else if (userAnswer == 1) {
            new FreqAnalysis();
        } else {
            System.out.println("Bad input");
        }
    }

    /**
     * Считать ответ пользователя. Тип ответа int.
     */
    private int readIntData() {
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

}
