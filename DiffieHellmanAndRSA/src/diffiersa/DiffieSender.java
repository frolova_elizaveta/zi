package diffiersa;

import java.math.BigInteger;
import java.util.ArrayList;

public class DiffieSender {
    /*Константа: имя файла для записи/чтения*/
    private final static String FILENAME = "file.txt";
    /*Константа: тип сообщения "генератор"*/
    public static final String GENERATOR_MSG = "generator";
    /*Константа: тип сообщения "простое число"*/
    public static final String PRIME_NUMBER_MSG = "primeNumber";
    /*Константа: тип сообщения "результат собеседника"*/
    public static final String COMPANION_RES_MSG = "companionResult";
    /*Объект класса, который позволяет считывать/записывать из/в txt-файл(а)*/
    private FileHelper myFileHelper;
    /*Генератор*/
    private int generator;
    /*Простое число*/
    private int primeNumber;
    /*Результат собеседника*/
    private int companionResult;
    /*Личный ключ*/
    private int privateKey;
    /*Вычисленный ключ, который должен совпасть с ключом собеседника*/
    private int myKey;
    /*Конструктор*/
    public DiffieSender(){
        /*Инициализация объекта класса FileHelper*/
        myFileHelper = new FileHelper();
    }
    /*Отправить сообщение: записать тип сообщения и его содержание в файл*/
    public void sendMessage(String type, int content){
        myFileHelper.writeData(type + " " + content, FILENAME);
    }

    /*Прочитать сообщение*/
    public void readMessage(){
        /*Читается последняя строка из списка считанных строк*/
        ArrayList<String> strings = myFileHelper.readData(FILENAME);
        String[] cur = strings.get(strings.size()-1).split(" ");
        /*Последняя строка из файла разбивается на две части: тип сообщения и его содержимое*/
        /*В зависимости от типа сообщения, соответствующей перменной присваивается содержимое сообщения*/
        switch(cur[0]){
            case GENERATOR_MSG:
                this.generator = (Integer.valueOf(cur[1]));
                break;
            case PRIME_NUMBER_MSG:
                this.primeNumber = (Integer.valueOf(cur[1]));
                break;
            case COMPANION_RES_MSG:
                this.companionResult = (Integer.valueOf(cur[1]));
                break;
        }
    }
    /*Вычисление выражения (base^power) mod divisor*/
    public int calculateMod(int base, int power, int divisor) {
        BigInteger p = new BigInteger(String.valueOf(base));
        p = p.pow(power);
        BigInteger r = new BigInteger(String.valueOf(divisor));
        p = p.mod(r);
        return p.intValue();
    }

    public int getGenerator() {
        return generator;
    }

    public void setGenerator(int generator) {
        this.generator = generator;
    }

    public int getPrimeNumber() {
        return primeNumber;
    }

    public void setPrimeNumber(int primeNumber) {
        this.primeNumber = primeNumber;
    }

    public int getCompanionResult() {
        return companionResult;
    }

    public void setCompanionResult(int companionResult) {
        this.companionResult = companionResult;
    }

    public int getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(int privateKey) {
        this.privateKey = privateKey;
    }

    public int getMyKey() {
        return myKey;
    }

    public void setMyKey(int myKey) {
        this.myKey = myKey;
    }
}
