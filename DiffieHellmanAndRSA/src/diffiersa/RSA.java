package diffiersa;

import java.util.ArrayList;

public class RSA {
    /*Константа: тип сообщения "генератор"*/
    public static final String N_MSG = "n";
    public static final String E_MSG = "e";
    public static final String TEXT_MSG = "text";
    private FileHelper myFileHelper;
    public RSA(){
        /*Создать двух собеседников, общающихся по открытому каналу*/
        /*Открытый канал - файл txt, в который они отправляют и из которого считывают сообщения друг друга*/
        RSASender firstSender = new RSASender();
        RSASender secondSender = new RSASender();
        /*Массив символов*/
        ArrayList<Character> chars = new ArrayList<>();
        /*Сначала один из собеседников генерирует числа*/
        firstSender.calculateNumbers();
        /*Отправить последовательно собеседнику два сгенерированных числа*/
        firstSender.sendMessage(N_MSG, firstSender.getBigNum());
        secondSender.readMessage();
        firstSender.sendMessage(E_MSG, firstSender.getExp());
        secondSender.readMessage();
        /*Заполнить массив символов символами из файла-сообщения*/
        myFileHelper = new FileHelper();
        chars = myFileHelper.readFile("message.txt");
        /*Список чисел - шифр текста - он равен тому, что зашифрует собеседник*/
        ArrayList<Integer> messageFromCompanion = secondSender.cryptText(chars);
        /*Записать зашифрованное сообщение в файл, чтобы можно было посмотреть*/
        myFileHelper.writeFileInt(messageFromCompanion, "firstSenderReceived.txt");
        /*Пересылка символов*/
        for(int msg : messageFromCompanion){
            secondSender.sendMessage(TEXT_MSG, msg);
            firstSender.readMessage();
        }
        /*Первый собеседник расшифровывает полученное сообщение*/
        /*Для этого он обрабатывает по формуле все символы, которые получил*/
        /*Он хранил их в переменной receivedMessages*/
        int i = 0;
        while(i < firstSender.receivedMessages.size()){
            chars.add(firstSender.encryptText(firstSender.receivedMessages.pollFirst()));
            i++;
        }
        /*Записать в файл расшифрованный текст*/
        myFileHelper.writeFile(chars, "firstSenderEncrypted.txt");

        new StartingPoint();
    }
}
