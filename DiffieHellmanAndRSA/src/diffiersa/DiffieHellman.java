package diffiersa;

public class DiffieHellman {
    /*Константа: тип сообщения "генератор"*/
    public static final String GENERATOR_MSG = "generator";
    /*Константа: тип сообщения "простое число"*/
    public static final String PRIME_NUMBER_MSG = "primeNumber";
    /*Константа: тип сообщения "результат собеседника"*/
    public static final String COMPANION_RES_MSG = "companionResult";
    public DiffieHellman(){
        /*Создать двух собеседников, общающихся по открытому каналу*/
        /*Открытый канал - файл txt, в который они отправляют и из которого считывают сообщения друг друга*/
        DiffieSender firstSender = new DiffieSender();
        DiffieSender secondSender = new DiffieSender();

        /*Генератор будет равен трём. Генератор отсылается, собеседник принимает его*/
        firstSender.setGenerator(3);
        firstSender.sendMessage(GENERATOR_MSG, firstSender.getGenerator());
        secondSender.readMessage();
        /*Простое число 17. Простое число отсылается, собеседник принимает его*/
        firstSender.setPrimeNumber(17);
        firstSender.sendMessage(PRIME_NUMBER_MSG, firstSender.getPrimeNumber());
        secondSender.readMessage();
        /*Личный ключ 54. Отсылается результат выражения 3^54 mod 17, собеседник принимает его*/
        firstSender.setPrivateKey(54);
        firstSender.sendMessage(COMPANION_RES_MSG,
                firstSender.calculateMod(firstSender.getGenerator(),firstSender.getPrivateKey(),firstSender.getPrimeNumber()));
        /*Личный ключ второго собеседника 24. Отсылается результат выражения 3^24 mod 17*/
        /*Собеседник принимает его*/
        secondSender.setPrivateKey(24);
        secondSender.sendMessage(COMPANION_RES_MSG,
                secondSender.calculateMod(secondSender.getGenerator(),secondSender.getPrivateKey(),secondSender.getPrimeNumber()));
        /*Наконец, каждый из собеседников вычисляет общий ключ. Если они совпадают, отображается соответствующее сообщение*/
        firstSender.setMyKey(firstSender.calculateMod(firstSender.getCompanionResult(), firstSender.getPrivateKey(), firstSender.getPrimeNumber()));
        secondSender.setMyKey(secondSender.calculateMod(secondSender.getCompanionResult(), secondSender.getPrivateKey(), secondSender.getPrimeNumber()));
        if(firstSender.getMyKey() == secondSender.getMyKey()){
            System.out.println("Keys are equal");
        }else{
            System.out.println("Keys are not equal");
        }
        /*Вернуться к начальному интерфейсу*/
        new StartingPoint();
    }
}
