package auth;

/**
 * Пользователь с полями логин, соль и хэш и соответствующими
 * геттерами и сеттерами.
 */
public class User {
    /*Логин*/
    private String login;
    /*Соль*/
    private String salt;
    /*Хэш соли и пароля*/
    private String hash;

    public String getHash() {
        return hash;
    }

    public String getSalt() {
        return salt;
    }

    public String getLogin() {
        return login;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}