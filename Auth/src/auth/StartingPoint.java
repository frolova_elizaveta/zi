package auth;

import java.util.Scanner;

/**
 * Начальный интерфейс
 */
public class StartingPoint {
    /*Конструктор начального интерфейса, который запускается в начале программы*/
    public StartingPoint() {
        System.out.println("0 - Registration\n" + "1 - Authorization");
        /*Получить ответ пользователя*/
        int userAnswer = readIntData();

        if (userAnswer == 0) {
            new Registration();
        } else if (userAnswer == 1) {
            new Authorization();
        } else {
            System.out.println("Bad input");
        }
    }

    /**
     * Считать ответ пользователя. Тип ответа int.
     */
    private int readIntData() {
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

}
