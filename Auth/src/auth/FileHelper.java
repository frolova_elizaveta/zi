package auth;

import java.io.*;
import java.util.ArrayList;

/**
 * Класс, позволяющий записать/считать данные о пользователях в/из txt-файл(а).
 * При создании объекта данного класса, на основе имеющегося txt-файла формируется
 * список пользователей с информацией о них - users.
 */
public class FileHelper {
    /*Название txt-файла*/
    private static final String FILENAME = "users.txt";
    /*Константа: Количество строк, которое отводится на одного пользователя*/
    private static final int USER_INFO_SIZE = 3;
    /*Константа: Номер строки, в которой хранится логин пользователя*/
    private static final int LOGIN_NUMBER = 0;
    /*Константа: Номер строки, в которой хранится соль пользователя*/
    private static final int SALT_NUMBER = 1;
    /*Константа: Номер строки, в которой хранится хэш пользователя*/
    private static final int HASH_NUMBER = 2;
    /*Объекты классов, неоходимые для записи и считывания данных*/
    private FileWriter myFileWriter;
    private BufferedWriter myBufferedWriter;
    private FileReader myFileReader;
    private BufferedReader myBufferedReader;
    /*Список строк из файла, содержащего данные о пользователях*/
    private ArrayList<String> userStrings;
    /*Список пользователей*/
    private ArrayList<User> users;

    /**
     * Конструктор класса, который вызывается при создании объекта класса
     */
    public FileHelper() {
        /*Инициализация списка строк*/
        userStrings = new ArrayList<>();
        /*Считать данные о пользователях из файла*/
        userStrings = readUser();
        if (userStrings != null) {
            /*Инициализация списка пользователей*/
            users = new ArrayList<>();
            /*Временная переменная: текущий пользователь*/
            User currentUser = new User();
            /*Цикл по всем строкам файла*/
            for (int i = 0; i < userStrings.size(); i++) {
                /*Создавать нового пользователя каждые три строки*/
                /*Если строка = 3*i + 0, то это логин: записать его как логин
                * текущего пользователя*/
                if ((i + USER_INFO_SIZE) % USER_INFO_SIZE == LOGIN_NUMBER) {
                    currentUser = new User();
                    currentUser.setLogin(userStrings.get(i));
                    /*Если строка = 3*i + 1, то это соль: записать её как соль
                    * текущего пользователя*/
                } else if ((i + USER_INFO_SIZE) % USER_INFO_SIZE == SALT_NUMBER) {
                    currentUser.setSalt(userStrings.get(i));
                    /*Если строка = 3*i + 2, то это хэш: записать его как хэш
                    * текущего пользователя*/
                    /*Если считан хэш, значит, записана вся информация о текущем
                    * пользователе. После этого необходимо добавить его в список
                    * пользователей.*/
                } else if ((i + USER_INFO_SIZE) % USER_INFO_SIZE == HASH_NUMBER) {
                    currentUser.setHash(userStrings.get(i));
                    users.add(currentUser);
                }
            }
        }
    }

    /**
     * Записать информацию о новом пользователе.
     */
    public void writeUser(String login, String password) {
        try {
            /*Запись будет производиться в файмл с указанным именем.
            * Второй параметр указывает на то, что данные будет записываться
            * без удаления предыдущих записей в файле.*/
            myFileWriter = new FileWriter(FILENAME, true);
            myBufferedWriter = new BufferedWriter(myFileWriter);
            /*Сгенерировать соль для пользователя*/
            String salt = Generator.generateSalt();
            /*Сгенерировать хэш для пользователя на основе его пароля и соли*/
            String hash = Generator.generateHash(password, salt);
            /*Записать последовательно логин, соль и хэш*/
            myBufferedWriter.write(login + "\n");
            myBufferedWriter.write(salt + "\n");
            myBufferedWriter.write(hash + "\n");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                myBufferedWriter.flush();
                myBufferedWriter.close();
                myFileWriter.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Считать информацию, записанную в txt-файле.
     * Считанные данные переводятся в список строк.
     */
    private ArrayList<String> readUser() {
        /*Временная переменная для хранения списка строк*/
        ArrayList<String> tempArray = new ArrayList<String>();
        try {
            /*Запись будет производиться из файла с указанным именем*/
            myFileReader = new FileReader(FILENAME);
            myBufferedReader = new BufferedReader(myFileReader);
            while (true) {
                String line = myBufferedReader.readLine();
                if (line == null) break;
                /*Разбить считанные данные по символу новой строки*/
                String[] parts = line.split("\n");
                /*Записать считанный массив в список данных о пользователях*/
                for (String str : parts) {
                    tempArray.add(str);
                }
            }
            return tempArray;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            /*Сообщение об ошибке*/
        } finally {
            try {
                myBufferedReader.close();
                myFileReader.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Получить список пользователей
     */
    public ArrayList<User> getUsers() {
        return users;
    }
}
