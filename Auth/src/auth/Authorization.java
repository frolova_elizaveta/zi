package auth;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Авторизация пользователя
 */
public class Authorization {

    private FileHelper myFileHelper;
    /*Список поьзователей*/
    ArrayList<User> users;
    private String userLogin;
    private String userPassword;

    /**
     * Конструктор класса
     */
    public Authorization() {
        /*Инициализация объекта класса FileHelper*/
        myFileHelper = new FileHelper();
        /*Инициализация списка пользователей на основе списка пользователей
        * из объекта класса FileHelper*/
        users = myFileHelper.getUsers();
        System.out.println("Enter login, please: ");
        String userLogin = readStringData();
        System.out.println("Enter password, please: ");
        String userPassword = readStringData();
        if (!users.isEmpty()) {
            /* "Совпали ли логин и пароль,
            * введённые пользователем, с логином и паролем зарегистрированного пользователя?"*/
            boolean successfully = false;
            /*Последовательный перебор всех пользователей*/
            for (User user : users) {
                /*Если логин, введённый пользователем, совпал с логином одного из
                 *зарегистрированных пользователей, то проверить, совпадает ли хэш
                 *зарегистрированного пользователя с хэшем, взятым от пароля, введённого
                 *пользователем, и соли зарегистрированного пользователя.*/
                if (user.getLogin().equals(userLogin)) {
                    if (Generator.generateHash(userPassword, user.getSalt()).equals(user.getHash())) {
                        successfully = true;
                    }
                }
            }
            if (successfully) {
                System.out.println("Congratulations! Authorization succeed");
            } else {
                System.out.println("Sorry, login or password incorrect.");
            }
        } else {
            System.out.println("Sorry! No signed up users!");
        }
        /*Вернуться к начальному интерфейсу*/
        new StartingPoint();
    }

    /**
     * Считать ответ пользователя. Тип ответа String.
     */
    private String readStringData() {
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }
}
