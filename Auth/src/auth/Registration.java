package auth;

import java.util.Scanner;

/**
 * Регистрация пользователя
 */
public class Registration {

    private FileHelper myFileHelper;
    private String userLogin;
    private String userPassword;

    /**
     * Конструктор регистрации, который запускается при событии "Регистрация"
     */
    public Registration() {
        myFileHelper = new FileHelper();
        System.out.println("Enter login, please: ");
        String userAnswer = readStringData();
        /*Если логин свободен, то*/
        if (!checkLogin(userAnswer)) {
            /*Запомнить логин пользователя*/
            userLogin = userAnswer;
            /*Дважды запросить пароль*/
            System.out.println("Enter password, please: ");
            /*Запомнить первый введённый пароль*/
            userPassword = readStringData();
            System.out.println("Repeat password, please: ");
            /*Если пароли совпадают, то*/
            if (userPassword.equals(readStringData())) {
                /*Зарегистрировать пользователя и отобразить сообщение
                 *об успешной регистрации */
                myFileHelper.writeUser(userLogin, userPassword);
                System.out.println("Thank you! I have successfully signed up!");
            } else {
                /*Если пароли не совпадают, то отобразить соответствующее сообщение*/
                System.out.println("Ooops! Passwords do not match!");
            }
        } else {
            /*Если логин занят, то отобразить соответствующее сообщение*/
            System.out.println("Sorry, this login is not available.");
        }
        /*Вернуться к начальному интерфейсу*/
        new StartingPoint();
    }

    /**
     * Считать ответ пользователя. Тип ответа String.
     */
    private String readStringData() {
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }

    /**
     * Проверить, свободен ли введённый пользователем логин
     */
    private boolean checkLogin(String login) {
        /*Последовательный перебор всех пользователей*/
        for (User user : myFileHelper.getUsers()) {
            /*Если введённый пользователем логин совпал с логином,
             *то вернуть значение true - логин занят. */
            if (user.getLogin().equals(login)) {
                return true;
            }
        }
        /*Иначе вернуть false - логин свободен. */
        return false;
    }
}
